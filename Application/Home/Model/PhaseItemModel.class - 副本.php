 <?php
namespace Home\Model;
use Think\Model;

class SearchModel extends Model
{
 	public function searchProject($name, $end_time){
 		if(empty($name) and empty($end_time) ){
			return false;
		}
		$model = M('project');
		if(!empty($name) ){
			$where['name'] = array('like', '%$name%');
		}
		if(!empty($end_time) ){
			$where['end_time'] = array('like', '%$end_time%');
		}
		$where['_logic'] = 'and';
		$map['_complex'] = $where;

		return $model->where($map)->select();
 	}


 }