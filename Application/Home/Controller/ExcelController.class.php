<?php
namespace Home\Controller;
use Think\Controller;
define('APP_DEBUG',False);
class ExcelController extends Controller {
    public function index(){
    	//$this->display();
    	// $dbname = $_GET["dbname"]; 
    	// $start = $_GET["start"];
    	// $len = $_GET["len"];
    	// $field = explode(",", $_GET["field"]);
    	// $filename = $_GET["filename"];
    	// $data = M($dbname)->field($field)->limit($start,$len)->select();
        if (IS_GET){
            $Proj = D("Project");
            $data = array('data' => $Proj->getProjList(),
                'item' => array('ID', '进度', '项目名称', '开始时间', '结束时间', '描述', '备注', '创建时间'),
                'filename' => "项目列表");
            $this->exportexcel($data['data'], $data['item'], $data['filename']);
        }
    }

    /**
    * 导出数据为excel表格
    *@param $data    一个二维数组,结构如同从数据库查出来的数组
    *@param $title   excel的第一行标题,一个数组,如果为空则没有标题
    *@param $filename 下载的文件名
    *@examlpe 
    $stu = M ('User');
    $arr = $stu -> select();
    exportexcel($arr,array('id','账户','密码','昵称'),'文件名!');
*/
 private function exportexcel($data=array(),$title=array(),$filename='report'){
    header("Content-type:application/octet-stream");
    header("Accept-Ranges:bytes");
    header("Content-type:application/vnd.ms-excel");  
    header("Content-Disposition:attachment;filename=".$filename.".xls");
    header("Pragma: no-cache");
    header("Expires: 0");
    //导出xls 开始
    if (!empty($title)){
        foreach ($title as $k => $v) {
            $title[$k]=iconv("UTF-8", "GB2312",$v);
        }
        $title= implode("\t", $title);
        echo "$title\n";
    }
    if (!empty($data)){
        foreach($data as $key=>$val){
            foreach ($val as $ck => $cv) {
                $data[$key][$ck]=$cv;//iconv("UTF-8", "GB2312", $cv);
            }
            $data[$key]=implode("\t", $data[$key]);
            
        }
        echo implode("\n",$data);
    }
 }

}