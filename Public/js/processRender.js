/**
*By why
*2014/5/27	
*/
	var fixedProcess = Array();
	var projid = 0;
	var edittable = false;
	//index为被选中的阶段在fixedProcess中的序号
	function deletePhase(index)
	{
		var prevPhase = findIndexById(fixedProcess[index]["prev"]);
		var nextPhase = findIndexById(fixedProcess[index]["next"]);
		var prevParallelPhase = findIndexById(fixedProcess[index]["prevParallel"]);
		var nextParallelPhase = findIndexById(fixedProcess[index]["nextParallel"]);

		if(prevPhase!=-1)
			fixedProcess[prevPhase]['next'] = fixedProcess[index]["next"];
		if(nextPhase!=-1)
			fixedProcess[nextPhase]['prev'] = fixedProcess[index]["prev"];
		if(prevParallelPhase!=-1)
			fixedProcess[prevParallelPhase]['nextParallel'] = fixedProcess[index]["nextParallel"];
		if(nextParallelPhase!=-1)
		{
			fixedProcess[nextParallelPhase]['prevParallel'] = fixedProcess[index]["prevParallel"];
			//************
			if(isMain(index))
			{
				fixedProcess[nextParallelPhase]["prev"] = fixedProcess[index]["prev"];
				fixedProcess[nextParallelPhase]["next"] = fixedProcess[index]["next"];
				if(prevPhase!=-1)
					fixedProcess[prevPhase]['next'] = fixedProcess[nextParallelPhase]["id"];
				if(nextPhase!=-1)
					fixedProcess[nextPhase]['prev'] = fixedProcess[nextParallelPhase]["id"];
			}
		}
		if(isMain(index))
		{
			if(nextParallelPhase!=-1)
			{
				fixedProcess[nextParallelPhase]["trueIndex"] = fixedProcess[index]["trueIndex"];
			}
			else
			{
				while(nextPhase!=-1)
				{
					fixedProcess[nextPhase]["trueIndex"] -= 1;
					nextPhase = findIndexById(fixedProcess[nextPhase]["next"]);
				}
			}
		}

		fixedProcess.splice(index,1);
	}

	function addPhase(newPhase)
	{
		var prevPhase = findIndexById(newPhase["prev"]);
		var nextPhase = findIndexById(newPhase["next"]);
		var prevParallelPhase = findIndexById(newPhase["prevParallel"]);
		var nextParallelPhase = findIndexById(newPhase["nextParallel"]);
		if(prevPhase!=-1)
			fixedProcess[prevPhase]['next'] = newPhase['id'];
		if(nextPhase!=-1)
			fixedProcess[nextPhase]['prev'] = newPhase['id'];
		if(prevParallelPhase!=-1)
			fixedProcess[prevParallelPhase]['nextParallel'] = newPhase['id'];
		if(nextParallelPhase!=-1)
			fixedProcess[nextParallelPhase]['prevParallel'] = newPhase['id'];
		fixedProcess.push(newPhase);
	}

	//生成新的阶段对象,index为点击节点的序号 pos:1-之前添加,0-之后添加,2-之上添加,3-之下添加
	function createPhase(index,pos,name,description,remark)
	{
		//console.log(fixedProcess[index]);
		var newPhase = new Object();
		newPhase.id = getNextId();
		newPhase.pid = projid;
		newPhase.name = name;
		newPhase.description = description;
		newPhase.remark = remark;
		newPhase.prev=newPhase.prevParallel=newPhase.next=newPhase.nextParallel=0;
		if(pos==1)
		{
			newPhase.prevParallel = fixedProcess[index]['prevParallel'];
			newPhase.nextParallel = fixedProcess[index]['id'];
			newPhase.trueIndex = fixedProcess[index]['trueIndex'];
			//**************
			if(isMain(index))
			{
				newPhase.prev = fixedProcess[index]["prev"];
				newPhase.next = fixedProcess[index]["next"];
				fixedProcess[index]["prev"] = 0;
				fixedProcess[index]["next"] = 0;
				fixedProcess[index]['prevParallel'] = newPhase.id;
			}
		}
		else if(pos==0)
		{
			newPhase.trueIndex = fixedProcess[index]['trueIndex'];
			newPhase.prevParallel = fixedProcess[index]['id'];
			newPhase.nextParallel = fixedProcess[index]['nextParallel'];
		}
		else if(pos==2&&(fixedProcess[index]['prev']!=0||fixedProcess[index]['next']!=0))
		{
			newPhase.trueIndex = fixedProcess[index]['trueIndex'];
			newPhase.prev = fixedProcess[index]['prev'];
			newPhase.next = fixedProcess[index]['id'];
			while(fixedProcess[index]['next']!=0)
			{
				fixedProcess[index]['trueIndex'] += 1;
				index = findIndexById(fixedProcess[index]['next']);
			}
			fixedProcess[index]['trueIndex'] += 1;
		}
		else if(pos==3&&(fixedProcess[index]['prev']!=0||fixedProcess[index]['next']!=0))
		{
			newPhase.prev = fixedProcess[index]['id'];
			newPhase.next = fixedProcess[index]['next'];
			if(fixedProcess[index]['next']==0)
			{
				newPhase.trueIndex = fixedProcess[index]['trueIndex']+1;
			}
			else
			{
				index = findIndexById(fixedProcess[index]['next']);
				newPhase.trueIndex = fixedProcess[index]['trueIndex'];
				while(fixedProcess[index]['next']!=0)
				{
					fixedProcess[index]['trueIndex'] += 1;
					index = findIndexById(fixedProcess[index]['next']);
				}
				fixedProcess[index]['trueIndex'] += 1;
			}
		}

		return newPhase;
	}

	function getNextId()
	{
		var i=0;
		for(var key in fixedProcess)
		{
			var value = fixedProcess[key];
			if(i<value['id'])
				i = value['id'];
		}
		return ++i;
	}

	function findIndexById(Id)
	{
		var i=0;
		for(var key in fixedProcess)
		{
			var value = fixedProcess[key];
			if(Id == value["id"])
				return i;
			i++;
		}
		return -1;
	}

	function isMain(index)
	{
		var phase = fixedProcess[index];
		if(phase==undefined)
			return false;
		else if(phase['prev']!=0||phase['next']!=0)
		{
			return true;
		}
		else
			return false;
	}

	function findTrueIndex(Phase,i)
	{
		if(Phase['prev']==0)
			return i;
		else
		{
			i++;
			i = findTrueIndex(fixedProcess[findIndexById(Phase['prev'])],i);
		}
		return i;
	}


	function getLogicX(Phase,x)
	{
		if(Phase['prevParallel']==0)
			return x;
		else
		{
			x++;
			x = getLogicX(fixedProcess[findIndexById(Phase['prevParallel'])],x);
		}
		return x;
	}

	function getLogicY(Phase,y)
	{
		if(Phase['prevParallel']==0)
			return 	Phase["trueIndex"];//findTrueIndex(Phase,0);
		else
		{
			y = getLogicY(fixedProcess[findIndexById(Phase['prevParallel'])],y);
		}
		return y;
	}

	function initPhase(projId,processJson,editable)
	{
		fixedProcess = $.parseJSON(processJson);
		projid = projId;
		edittable = editable
	}

	function renderProcess(canvas,item,originX,originY)
	{
		var canvas = $("#"+canvas);
		canvas.html("");
		var item = $("#"+item);
		var width = item.width();
		var height = item.height();
		for(var key in fixedProcess)
		{
			var value = fixedProcess[key];
			fixedProcess[key]["id"] = parseInt(fixedProcess[key]['id']);
			fixedProcess[key]["trueIndex"] = parseInt(fixedProcess[key]['trueIndex']);
			var LX = getLogicX(value,0);
			var LY = getLogicY(value,0);
			X = LX*width;
			Y = LY*height;
			printDiv(canvas,item.clone(true),X,Y,value['name'],originX,originY,value,key);
		}
	}

	function printDiv(canvas,item,X,Y,name,originX,originY,phase,index)
	{
		X += originX;
		Y += originY;

		item.attr("index",index);
		item.attr("phaseId",phase['id']);
		if(phase['finished']==1)
			item.find("div").css("background-color","green");
		else
			item.find("div").css("background-color","gray");
		if(edittable)
			item.find("div").css("border","3px orange solid");
		item.css("left",X+"px");
		item.css("top",Y+"px");
		item.css("position","absolute");
		item.css("display","");
		item.find("a").eq(0).attr("href",phase['jumpUrl']);
		item.find("div").html(name);

		canvas.append(item);
	}