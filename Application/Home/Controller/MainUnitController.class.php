<?php
namespace Home\Controller;
use Think\Controller;

class MainUnitController extends BaseController
{
	public function index()
	{
		if(empty($_GET['project_id']))
		{
			echo "请选择项目";
		}
		else
		{
			if(!empty($_GET['category_id'])&&$_GET['category_id']==2)
			{
				$User = D("User");
				if(!$User->hasPrivilege(array("projects"=>array($_GET['project_id']=>array("finance")))))
				{
					echo "无权限";
					//$this->error("无权限");
					exit;
				}
			}
			$phasename = D("Phase")->where(array('phase_id'=>$_GET['phase_id']))->field("name,finished")->find();
			$projname = D("Project")->where(array('id'=>$_GET['project_id']))->field("name")->find();
			$this->assign("phase_name",$phasename['name']);
			$this->assign("finished",$phasename['finished']);
			$this->assign("project_name",$projname['name']);
			$this->display();
		}
	}
}