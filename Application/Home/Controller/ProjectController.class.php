<?php
namespace Home\Controller;
use Think\Controller;

class ProjectController extends BaseController
{
	public function createProj()
	{
		$Proj = D("Project");
		$Phase = D("Phase");
		if(IS_POST)
		{
			$PK;
			$data = array();
			if($_POST['type']=='edit')
			{
				$User = D("User");
				if(!$User->hasPrivilege(array("projects"=>array($_GET['id']=>array("normal")))))
				{
					$this->error("无权限");
					exit;
				}
				unset($_POST['type']);	
				if(!$Proj->updateProj())
				{
					$this->error("项目编辑失败 ".$Proj->getError());	
				}
				else
				{
					//用户历史记录
					D("User")->addHistory("编辑了项目基本信息，项目id:".$_GET['id']);

					$this->success("编辑成功");
				}
			}
			else
			{
				if(!($PK=$Proj->createProj()))
				{
					$this->error("项目创建失败 ".$Proj->getError());
				}	
				else
				{
					if(!$Phase->createProjPhase($PK,$Phase->getFixedProcess($PK)))
					{
						$this->error("项目流程创建失败 ".$Phase->getError());	
					}
					else
					{
						//用户历史记录
						D("User")->addHistory("创建了项目:".$_POST['name']);

						$this->success("创建成功",U("Index/board"));
					}
				}
			}
		}
		else
		{
			if($_GET['type']=="edit")
			{
				$this->assign("projInfo",D('Project')->where(array('id'=>$_GET['project_id']))->find());
			}
			$this->display();
		}
	}

	public function listProj()
	{
		$Proj = D("Project");
		// var_dump($Proj->getProjList()); 
		$proj_list = $Proj->getProjList();
		$this->assign("projList", $proj_list);
		$this->display();
	}

	public function mainlistProj()
	{
		$Proj = D("Project");
		$Phase = D("Phase");
		$User = D("User");
		$proj_list = $Proj->getProjList();
		foreach($proj_list as $key=>$value)
		{
			$proj_list[$key]['progress'] = $Proj->getProjProgress($value['id']);
			$proj_list[$key]['phase'] = $Phase->where(array('phase_id'=>$value['phase_id']))->find();
			$proj_list[$key]['popurl'] = U('Project/createProj',array('type'=>'edit','project_id'=>$value['id']));
		} 
		$this->assign("projList", $proj_list);
		$this->display();
	}

	public function deleteProj()
	{
		$User = D("User");
		if(!$User->hasPrivilege(array("projects"=>array($_GET['project_id']=>array("normal")))))
		{
			$this->error("无权限");
			exit;
		}

		$Proj = D("Project");
		if(!$Proj->where(array('id'=>$_GET['project_id']))->delete())
			$this->error("删除失败".$Proj->getError());
		else
			$this->success("删除成功");
	}
}