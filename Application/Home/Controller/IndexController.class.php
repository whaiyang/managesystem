<?php
namespace Home\Controller;
use Think\Controller;
class IndexController extends BaseController {
    public function index()
    {
    	if(empty($_SESSION['udata']))
    		$this->display(); 
    	else
    		$this->redirect("Index/main");   
    }

    public function login()
	{
        if(!empty($_SESSION["udata"]))
        {
            $this->redirect(U("Index/main"));
            return;
        }
		$user = D("user");
		$udata = $user->where(array("username"=>$_POST["username"],"password"=>md5($_POST['password']),"status"=>array("neq",0)))->find();
        if(empty($udata))
        {
            //用户历史记录
            D("User")->addHistory("登陆失败");

			$this->error("用户名或密码错误");
        }
		else
		{
            $g = D('UserGroup')->where(array('group_id'=>$udata['group_id']))->find();
            $udata['admin'] = $g['admin'];
            $user->where(array("username"=>$_POST["username"],"password"=>md5($_POST['password']),"status"=>array("neq",0)))->save(array("last_login_time"=>time(),"last_login_ip"=>get_client_ip()));
			$user->where(array("username"=>$_POST["username"],"password"=>md5($_POST['password']),"status"=>array("neq",0)))->setInc("login_num",1);
            $_SESSION['udata'] = $udata;
            //用户历史记录
            D("User")->addHistory("登陆成功");

			$this->success("登录成功");
		}
	}	

    public function loginout()
    {
        //用户历史记录
        D("User")->addHistory("登出");
    	unset($_SESSION['udata']);

    	$this->redirect("Index/index");
    }

    public function main()
    {
		$this->display();
    }

    public function none()
    {
        $this->display();
    }
}