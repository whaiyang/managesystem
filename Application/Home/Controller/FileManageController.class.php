<?php
    namespace Home\Controller;
    use Think\Controller;

class FileManageController extends BaseController
{
	public function downloadFile()
	{
		if(IS_GET)
		{
			//用户历史记录
			D("User")->addHistory("下载了文件".$_GET['downloadFileName']);

			if(!empty($_GET['downloadFileName']))
			{
				$downloadFileName = $_GET["downloadFileName"];
				$FileManage = D('FileManage');
				$arr = $FileManage->downloadFile($downloadFileName);
				force_download($arr['name'],file_get_contents($arr['path']));
			}
		}
		
	}

	public function addfile()
	{//http://localhost/managesystem/index.php/Home/FileManage/addfile.html?phase_id=1&project_id=1&category_id=1&type=0
		if(IS_POST)
		{
			$User = D("User");
			if(!$User->hasPrivilege(array("projects"=>array($_GET['project_id']=>array("normal")))))
			{
				$this->error("无权限");
				exit;
			}
			$phase_id 		= 	$_GET['phase_id'];
			$project_id 	= 	$_GET['project_id'];
			$category_id 	= 	$_GET['category_id'];
			$name 			=	$_POST['inputFileName'];
			$type 			= 	$_GET['type'];
			$FileManage = D('FileManage');
			$return = $FileManage->uploadFile($phase_id, $project_id, $category_id, $name, $type);
			if($return==false)
			{
				$this->error($FileManage->getError());
				return;
			}
			elseif($return == true)
			{
				//用户历史记录
				D("User")->addHistory("上传了文件".$name.$type);

				$this->success('文件上传成功');
			}

		}
		else
		{
			$this->display();
		}
	}

	public function deleteFile()
	{
		if(IS_GET)
		{
			$User = D("User");
			if(!$User->hasPrivilege(array("projects"=>array($_GET['project_id']=>array("normal")))))
			{
				$this->error("无权限");
				exit;
			}
			
			$deleteFileName = $_GET['deleteFileName'];

			$FileManage = D('FileManage');
			$return = $FileManage->deleteFile($deleteFileName);
			if($return == true)
			{
				//用户历史记录
				D("User")->addHistory("删除了文件".$FileManage);

				$this->success('文件删除成功');
			}
			else
			{
				$this->error('文件删除失败');
			}

		}
	}

	public function file()
	{
		if(IS_GET)
		{
			$phase_id 	= 	$_GET['phase_id'];
			$project_id 	= 	$_GET['project_id'];
			$category_id 	= 	$_GET['category_id'];
			if((!empty($phase_id)) and (!empty($project_id) ) ){
				$FileManage = D('FileManage');
				$return = $FileManage->findFileWithPhase($phase_id);
				$this->assign("returnFile",$return);
			}
			elseif ( (!empty($project_id) ) and (!empty($category_id) ) ) {
				$FileManage = D('FileManage');
				$return = $FileManage->findFileWithProject($project_id,$category_id);
				$this->assign("returnFile",$return);
			}
			$this->display();
		}
	}


}