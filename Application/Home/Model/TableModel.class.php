<?php
namespace Home\Model;
use Think\Model;

class TableModel extends Model
{
	protected $_validate = array(
		array("project_id","require","数据错误",self::MUST_VALIDATE),
		array("name","require","请输入名字",self::MUST_VALIDATE),
		);

	protected $_auto = array (
		array("time","time",self::MODEL_INSERT,"function"),
		array("username","getUsername",self::MODEL_INSERT,"callback"),
		);

	public function getUsername($value)
	{
		return $_SESSION['udata']['username'];
	}
}