<?php
namespace Home\Controller;
use Think\Controller;

class UserGroupController extends BaseController
{
	public function index()
	{
		if(!D("User")->isAdmin())
		{
			echo "无权限";
			exit;
		}

		$group = D("UserGroup");
		$groupList = $group->where("")->select();
		$this->assign("groupList",$groupList);
		$this->display();
	}

	public function add()
	{
		if(!D("User")->isAdmin())
		{
			echo "无权限";
			exit;
		}

		if(IS_POST)
		{
			$group = D("UserGroup");
			$this->handlePrivilege();
			if(!$group->create())
				$this->error($group->getError());
			else
			{
				if(!$group->add())
					$this->error($group->getError(),U("UserGroup/index"));	
				else
				{
					//用户历史记录
					D("User")->addHistory("添加了用户组".$_POST['name']);

					$this->success("添加成功",U("UserGroup/index"));
				}
			}
		}
		else
		{
			$projList = D("Project")->where("")->select();
			$this->assign("submitUrl",U("UserGroup/".ACTION_NAME));
			$this->assign("gdata",array());
			$this->assign("privilege",array());
			$this->assign("projlist",$projList);
			$this->display();
		}
	}

	public function edit()
	{
		if(!D("User")->isAdmin())
		{
			echo "无权限";
			exit;
		}

		if(!$this->check_groupid())
			return;
		$group = D("UserGroup");
		if(IS_POST)
		{
			$this->handlePrivilege();
			if(!$group->create($_POST,2))
				$this->error($group->getError());
			else
			{
				if(!$group->save())
					$this->error($group->getError(),U("UserGroup/index"));	
				else
				{
					//用户历史记录
					D("User")->addHistory("编辑了用户组: ".$_POST['name']);

					$this->success("编辑成功",U("UserGroup/index"));
				}
			}
		}
		else
		{
			$projList = D("Project")->where("")->select();
			$groupdata = $group->where(array("group_id"=>$_GET["group_id"]))->find();
			$this->assign("gdata",$groupdata);
			$groupdata["privilege"] = str_replace("&quot;", "\"", $groupdata["privilege"]);
			$this->assign("submitUrl",U("UserGroup/".ACTION_NAME."?group_id=".$_GET['group_id']));
			$this->assign("privilege",unserialize($groupdata["privilege"]));
			$this->assign("projlist",$projList);
			$this->display("add");
		}
	}

	public function delete()
	{
		if(!D("User")->isAdmin())
		{
			echo "无权限";
			exit;
		}

		if(!$this->check_groupid())
			return;
		$group = D("UserGroup");
		if($group->deleteGroup($_GET["group_id"]))
		{
			//用户历史记录
			D("User")->addHistory("删除了用户组，id:".$_GET["group_id"]);

			$this->success("删除成功",U("UserGroup/index"));
		}
		else
			$this->error("删除失败",U("UserGroup/index"));
	}

	private function handlePrivilege()
	{
		$privilege = array();
		foreach($_POST['privilege'] as $key=>$value)
		{
			$privilege["$value"] = array();
			if(!empty($_POST['privilege_normal'][$key]))
				$privilege["$value"][] = "normal";
			if(!empty($_POST['privilege_finance'][$key]))
				$privilege["$value"][] = "finance";
		}
		$_POST['privilege'] = serialize($privilege);
		//echo $_POST['privilege'];
	}

	private function check_groupid()
	{
		$group_id = $_GET["group_id"];
		if(empty($group_id))
		{
			$this->error("数据错误,group_id未指定");
			return false;
		}
		else
		{
			$_POST["group_id"] = $group_id;
			return true;
		}
	}
}