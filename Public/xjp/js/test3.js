$(document).ready(function() {
        $('body').on('click','.tableselect .menu-1',function () {
                var e=$(this).next('.menu-2');
                var flag=$(this).hasClass("active");
                while($(e).length){
                    if ($(e).hasClass("active")==flag) {$(e).button('toggle'); }
                    e=$(e).next('.menu-2');
                }
        });

        $('body').on('click','.tableselect .menu-2',function () {
                var e=$(this).prevAll('.menu-1')[0];
                if ($(this).hasClass("active") && $(e).hasClass("active")) {
                    $(e).button('toggle'); 
                 }
                    
        });

   
/**************private function***********************/
    function isEmptyObject(obj){
            for(var n in obj){return false} 
            return true; 
        } 

    function createmenu (menutype,menuname) {
        return '<li  class="list-group-item btn btn-default menu-'+menutype+'"><input type="checkbox" value="'+menuname+'">'+menuname+'<span class="glyphicon glyphicon-ok"></span></li>';
    }



/******************insert menu to A table***************************/
    function creattablehead (menuarray,onetable) {
            $.each(menuarray,function (key,value) {
                $(onetable).find('ul').append(createmenu(1,key));
                if(!isEmptyObject(value)) {
                    $.each(value,function (key2) {
                        $(onetable).find('ul').append(createmenu(2,key2));
                    });
                }
            });
        }

/**************** create tables from a ARRAY of names*******************/
    function creattables (tablename) {
        var tmph='<div class="tableselect panel panel-default col-xs-4"><div class="panel-heading">';
        var tmpf='</div><ul class="list-group"  data-toggle="buttons" ></ul></div>';
        for (var i = 1; i < tablename.length+1; i++) {
            var tabletmp=tmph+tablename[i-1]+tmpf;
            $('.formfix').before(tabletmp);
            if (i % 3 ==0) {
                 $('.formfix').before('<div class="clearfix"></div>');
            };
        };
    }


/***************test data************************/
    /*var kkk={
             'aaa':[   
                        {'aaa':{},'bbb':{'ccc':{},'ddddd':{}},'eeeee':{}},
                        [['bbb','ccc'],['bbb','ccc'],['bbb','ccc'],['qqq','cggggcc']]
                    ],
             'vvv':[   
                        {'aaa':{},'bbb':{},'ccc':{'ddddd':{},'fff':{}},'eeeee':{}},
                        [['bbb','ccc'],['ssss','hhhh'],['bbb','ccc'],['bbb','ccc'],['vvvv','xxxxx']]
                    ]
             };*/

    var kkkt=$('body').attr('xjp-data');
    var kkk=eval('(' + kkkt + ')');


/**********************/
    var testname=[];
    var tablevalue=[];
    $.each(kkk,function  (n,v) {
        testname.push(n);
        tablevalue.push(v);
    })

/**********************/
    creattables(testname);

    var tableselect=$('.tableselect');
    for (var i = 0; i < tableselect.length; i++) {
        creattablehead(tablevalue[i][0],tableselect[i])
    };

    $('.tableselect .btn').button();

/*******************get the col number based on the table name and the menu name *******************/
    function getcol (curtable,ss) {
            var curcol=0;
            var tt=-1;
            var tmp=kkk[curtable][0];
              $.each(tmp,function (key,value) {
                if(isEmptyObject(value)) {
                    if(key==ss) {tt=curcol; return};
                    curcol++;
                }else{
                   $.each(value,function (key2) {
                        if(ss==key2 ) {tt=curcol; return};
                        curcol++;
                    });
                }
            });
            return tt;
    }


     $('#submitform>.btn-submit').click(function(){
            var newvalue=[];
            var newmenu={};


            var allselect=$('.tableselect .btn.active');
            for (var i = 0; i < $(allselect).length; i++) {
                var curmenu=$(allselect[i]).find('input').val();
                var curtable=$(allselect[i]).parents('.list-group').prev('.panel-heading').html();
                var curcol=getcol(curtable,curmenu);
                if (curcol+1) {
                    newvalue.push(kkk[curtable][1][curcol]);
                    newmenu[curmenu]={};
                };
            };


        /************ecode from tablevalue *********/
            var jsonnewvalue=JSON.stringify(newvalue);
            var jsonnewmenu=JSON.stringify(newmenu);

            $('#submitform>#newvalue').val(jsonnewvalue);
            $('#submitform>#newmenu').val(jsonnewmenu);

            // $('#submitform').submit(function () {
            //     alert('\u8868\u683C\u63D0\u4EA4\u6210\u529F\uFF01');
            //  });
         });
  });