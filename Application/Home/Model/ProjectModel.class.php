<?php
namespace Home\Model;
use Think\Model;

class ProjectModel extends Model
{
	protected $_validate = array(
		array("name","require","请输入项目名",self::MUST_VALIDATE),
		);

	protected $_auto = array (
		array("create_time","time",self::MODEL_INSERT,"function"),
		);

	//初始化固定流程和预期流程
	public function createProj()
	{
		$PK = null;
		//$this->startTrans();
		if(!$this->create())
			return false;
		if(!($PK=$this->add()))
			return false;
		/*$Phase = D("Phase");
		if(!$Phase->initProjPhase($PK))
		{
			$this->error = $Phase->getError();
			$this->rollback();
			return false;
		}
		$this->commit();*/
		return $PK;
	}

	public function updateProj()
	{
		if(!$this->create())
			return false;
		if(!$this->save())
			return false;
		return true;	
	}

	public function getCurrentProj()
	{
		return $_SESSION["curproj"];
	}

	public function getProjProgress($projid)
	{
		$Phase = D("Phase");
		$phaseNum = floatval($Phase->where(array("pid"=>$projid))->count());
		$finishedNum = floatval($Phase->where(array("pid"=>$projid,"finished"=>1))->count());

		return number_format($finishedNum/$phaseNum,3);
	}

	public function getProjList()
	{
		return $this->order("id desc")->select();
	}
}