<?php
namespace Home\Model;
use Think\Model;

class UserModel extends Model
{
	protected $_validate = array(
		array("group_id","require","请选择用户组",self::VALUE_VALIDATE),
		array("username","require","请输入用户名",self::MUST_VALIDATE),
		array("username","usernameLen","用户名最长20位",self::MUST_VALIDATE,"callback"),
		array('username','','帐号名称已经存在！',self::EXISTS_VALIDATE,'unique',self::MODEL_INSERT),
		array("password","require","请输入密码",self::MUST_VALIDATE,"",self::MODEL_INSERT),
		array("password","4,32","密码长度错误（最短4位，最长32位）",self::MUST_VALIDATE,"length",self::MODEL_INSERT),
		array("password","require","请输入密码",self::VALUE_VALIDATE,"",self::MODEL_UPDATE),
		array("password","4,32","密码长度错误（最短4位，最长32位）",self::VALUE_VALIDATE,"length",self::MODEL_UPDATE),
		array("status","0,1","状态数据错误",self::VALUE_VALIDATE,"in",self::MODEL_UPDATE),
		array("email","/^[0-9a-zA-Z]+@(([0-9a-zA-Z]+)[.])+[a-z]{2,4}$/i","邮箱地址格式不正确",self::VALUE_VALIDATE,"regex"),
		);

	protected $_auto = array (
		array("reg_time","time",self::MODEL_INSERT,"function"),
		array("password","",self::MODEL_UPDATE,"ignore"),
		array("password","md5",self::MODEL_INSERT,"function"),
		);

	public function usernameLen($value)
	{
		if(strlen($value)>20)
			return false;
		else
			return true;
	}

	public function deleteUser($uid)
	{
		return $this->where(array("user_id"=>$uid))->delete();
	}

	public function addHistory($operationName)
	{
		if(!empty($_SESSION["udata"]))
		{
			$history = M("history");
			$history->add(array("user_id"=>$_SESSION['udata']["user_id"],"username"=>$_SESSION["udata"]['username'],"operation"=>$operationName,"time"=>time(),"ip"=>get_client_ip()));
		}
	}

	/**
	*用户始终有浏览权限
	*表示某一用户的权限的数组，normal表示普通权限，finance表示拥有财务权限
	*privilegeArr = array("projects"=>array(
	*											"projectId1"=>array("normal","finance"),
	*											"projectId2"=>array("normal"),	
	*										)
	*					);
	*/
	public function hasPrivilege($privilegeArr)
	{
		if(empty($_SESSION['udata']))
			return false;
		$user_id = $_SESSION['udata']["user_id"];
		$model = new Model();
		$user_data_full = $model->table("ms_user as u,ms_user_group as g")->where("u.user_id=$user_id AND g.group_id=u.group_id")->field("g.*")->find();
		//若用户组不存在或被禁用则用户无权限
		if(empty($user_data_full)||$user_data_full["status"]==0)
			return false;
		//若用户组为超级管理组则用户始终有权限
		if($user_data_full["admin"]==1)
			return true;
		$parr = unserialize($user_data_full["privilege"]);
		$parr = $parr["projects"];
		foreach($privilegeArr["projects"] as $value)
		{
			$key = $value["group_id"];
			if(empty($parr[$key]))
				return false;
			if(in_array($value, $parr[$key]))
				continue;
			else
				return false;
		}
		return true;
	}

	public function isAdmin()
	{
		if(empty($_SESSION['udata']))
			return false;
		if($_SESSION['udata']['admin']==1)
			return true;
		else
			return false;
	}

	public function getCompetentProjId()
	{
		if(empty($_SESSION['udata']))
			return false;
		$privilege = D("UserGroup")->where(array('group_id'=>$_SESSION['udata']['group_id']))->find();
		$privilege = unserialize($privilege['privilege']);
		$ids = array();
		foreach($privilege as $key=>$value)
		{
			$ids[] = $key;
		}
		return $ids;
	}
}