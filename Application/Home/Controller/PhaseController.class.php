<?php
namespace Home\Controller;
use Think\Controller;

class PhaseController extends BaseController
{
	public function index()
	{
		$projId = $_GET["projid"];
		$Phase = D("Phase");
		$_SESSION['proj']['id'] = $projId;
		$fixedProcess = "";
		$submitUrl = "";
		$jumpUrl = "";
		if($_GET["type"]=="create")
		{
			$fixedProcess = $Phase->getFixedProcess($projId);
			$submitUrl = U("Phase/create?projid=$projId");
		}
		else if($_GET["type"]=="update")
		{
			$fixedProcess = $Phase->where(array("pid"=>$projId))->select();
			foreach($fixedProcess as $key=>$value)
			{
				$fixedProcess[$key]['jumpUrl'] = U('MainUnit/index',array('project_id'=>$_GET['projid'],'phase_id'=>$value['phase_id']));
			}
			$submitUrl = U("Phase/update?projid=$projId");
		}
		$fixedProcessStr = json_encode($fixedProcess);

		$editable = true;
		if(!empty($_GET['editable'])||$_GET['editable']==0)
			$editable = $_GET['editable'];

		$this->assign("editable",$editable);
		$this->assign("submitUrl",$submitUrl);
		$this->assign("projid",$projId);
		$this->assign("fixedProcessStr",$fixedProcessStr);
		$this->display();
	}

	public function create()
	{
		$User = D("User");
		if(!$User->hasPrivilege(array("projects"=>array($_GET['projid']=>array("normal")))))
		{
			$this->error("无权限");
			exit;
		}

		$projid = $_GET["projid"];
		$Phase = D("Phase");
		$phasearr = json_decode($_POST['phasearr'],true);
		if(!$Phase->createProjPhase($projid,$phasearr))
		{
			$data["status"] = 0;
			$data["info"] = $Phase->getError();
			$this->ajaxReturn($data);
		}
		else
		{
			$data["status"] = 1;
			$data["info"] = "创建成功";
			$this->ajaxReturn($data);
		}
	}

	public function update()
	{
		$User = D("User");
		if(!$User->hasPrivilege(array("projects"=>array($_GET['projid']=>array("normal")))))
		{
			$this->error("无权限");
			exit;
		}

		$projid = $_GET["projid"];
		$Phase = D("Phase");
		$phasearr = json_decode($_POST['phasearr'],true);
		foreach ($phasearr as $key => $value) {
			unset($phasearr[$key]['jumpUrl']);
		}
		if(!$Phase->updateProjPhase($projid,$phasearr))
		{
			$data["status"] = 0;
			$data["info"] = $Phase->getError();
			//$this->ajaxReturn($data);
			$this->error("编辑失败 ".$Phase->getError());
		}
		else
		{
			//用户历史记录
			D("User")->addHistory("编辑了流程，项目id:".$projid);

			$data["status"] = 1;
			$data["info"] = "编辑成功";
			//$this->ajaxReturn($data);
			$this->success("编辑成功");
		}
	}

	public function toggleFinished()
	{
		$User = D("User");
		if(!$User->hasPrivilege(array("projects"=>array($_GET['project_id']=>array("normal")))))
		{
			$this->error("无权限");
			exit;
		}

		$phase_id = $_GET['phase_id'];
		$project_id = $_GET['project_id'];
		D("Phase")->execute("update __PREFIX__phase set finished=ABS(finished-1) where phase_id=$phase_id");
		$this->redirect("MainUnit/index",array("project_id"=>$project_id,"phase_id"=>$phase_id,"refresh"=>1));
	} 
}