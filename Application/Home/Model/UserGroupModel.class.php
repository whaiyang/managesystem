<?php
namespace Home\Model;
use Think\Model;

class UserGroupModel extends Model
{
	protected $_validate = array(
		array("name","require","请输入用户名",self::MUST_VALIDATE),
		array('name','','帐号名称已经存在！',self::EXISTS_VALIDATE,'unique',self::MODEL_INSERT),
		array("status","0,1","状态数据错误",self::VALUE_VALIDATE,"in",self::MODEL_BOTH),
		array("admin","0,1","状态数据错误",self::VALUE_VALIDATE,"in",self::MODEL_BOTH),
		);

	protected $_auto = array (
		);

	public function deleteGroup($gid)
	{
		$result = $this->where(array("group_id"=>$gid))->delete();
		//将该组的所有用户移到默认用户组中
		if(!empty($result))
		{
			$this->table("ms_user")->where(array("group_id"=>$gid))->save(array("group_id"=>2));
		}
		return $result;
	}
}