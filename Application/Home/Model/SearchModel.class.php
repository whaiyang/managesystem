<?php
namespace Home\Model;
use Think\Model;

class SearchModel extends Model
{
 	public function searchProject($name, $address, $fund, $start_time, $end_time, $accordance, $source_of_funds, $progress){
 		if(empty($name) and empty($address) and empty($fund) and empty($start_time) and empty($end_time) and empty($accordance) and empty($source_of_funds) and empty($progress) ){
			return false;
		}
		$model = M('project');
		if(!empty($name) ){
			$condition['name'] = array('like', "%".$name."%");
		}
		if(!empty($address) ){
			$condition['address'] = array('like', "%".$address."%");
		}
		if(!empty($fund) ){
			$condition['fund'] = array('like', $fund);
		}
		if(!empty($start_time) ){
			$condition['start_time'] = array('like', $start_time);
		}
		if(!empty($end_time) ){
			$condition['end_time'] = array('like', $end_time);
		}
		if(!empty($accordance) ){
			$condition['accordance'] = array('like', "%".$accordance."%");
		}
		if(!empty($source_of_fund) ){
			$condition['source_of_funds'] = array('like', "%".$source_of_funds."%");
		}
		if(!empty($progress) ){
			$phase_id = $this->NameTophaseId($progress);
			$condition['phase_id'] = array('like', $phase_id);
		}

		$condition['_logic'] = 'and';
		$return  = $model->where($condition)->select();
		return $return;
 	}

 	public function phaseIdToName($phase_id){
 		if(empty($phase_id)){
 			return false;
 		}
 		$model = M('phase');
 		$return = $model->field('name')->where("phase_id=%d",$phase_id)->find();
 		return  $return['name'];
 	}

 	public function NameTophaseId($name){
 		if(empty($name)){
 			return false;
 		}
 		$model = M('phase');
 		$condition['name'] = array('like', "%".$name."%");
		$return  = $model->field('phase_id')->where($condition)->select();
		$return = $this->formatArray($return, 'phase_id');
 		return  $return;
 	}
	public function formatArray($array, $member){
		$return = array();
		foreach ((array)$array as $v) {
			$ret = array($v[$member]);
			$return = array_merge($return ,$ret);
		}
		return $return;
	}

 }