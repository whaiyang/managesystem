<?php
namespace Home\Model;
use Think\Model;

//数据库中以链表方式存储
class PhaseModel extends Model
{
	protected $_validate = array(
		array("name","require","请输入阶段名",self::MUST_VALIDATE),
		);

	//id,name,description,prev,next,prevParallel,nextParallel,remark,type,trueIndex
	protected $fixedProcess = array(
		array("id"=>1,"name"=>"项目批准依据","description"=>"描述","prev"=>0,"next"=>2,"prevParallel"=>0,"nextParallel"=>0,"remark"=>"备注","type"=>0,"trueIndex"=>0),
		array("id"=>2,"name"=>"办理项目预立项","description"=>"描述","prev"=>1,"next"=>3,"prevParallel"=>0,"nextParallel"=>0,"remark"=>"备注","type"=>0,"trueIndex"=>1),
		array("id"=>3,"name"=>"比选代理公司","description"=>"描述","prev"=>2,"next"=>4,"prevParallel"=>0,"nextParallel"=>18,"remark"=>"备注","type"=>0,"trueIndex"=>2),
		//\**
		array("id"=>18,"name"=>"比选造价咨询公司","description"=>"描述","prev"=>0,"next"=>0,"prevParallel"=>3,"nextParallel"=>0,"remark"=>"备注","type"=>0,"trueIndex"=>2),
		///**\
		array("id"=>4,"name"=>"勘察招标","description"=>"描述","prev"=>3,"next"=>5,"prevParallel"=>0,"nextParallel"=>19,"remark"=>"备注","type"=>0,"trueIndex"=>3),
		//\**/
		array("id"=>19,"name"=>"测绘招标","description"=>"描述","prev"=>0,"next"=>0,"prevParallel"=>4,"nextParallel"=>20,"remark"=>"备注","type"=>0,"trueIndex"=>3),
		array("id"=>20,"name"=>"设计招标","description"=>"描述","prev"=>0,"next"=>0,"prevParallel"=>19,"nextParallel"=>0,"remark"=>"备注","type"=>0,"trueIndex"=>3),
		///**\
		array("id"=>5,"name"=>"初设审查","description"=>"描述","prev"=>4,"next"=>6,"prevParallel"=>0,"nextParallel"=>0,"remark"=>"备注","type"=>0,"trueIndex"=>4),
		array("id"=>6,"name"=>"施工图设计","description"=>"描述","prev"=>5,"next"=>7,"prevParallel"=>0,"nextParallel"=>0,"remark"=>"备注","type"=>0,"trueIndex"=>5),
		array("id"=>7,"name"=>"图审","description"=>"描述","prev"=>6,"next"=>8,"prevParallel"=>0,"nextParallel"=>0,"remark"=>"备注","type"=>0,"trueIndex"=>6),
		array("id"=>8,"name"=>"优化评审","description"=>"描述","prev"=>7,"next"=>9,"prevParallel"=>0,"nextParallel"=>0,"remark"=>"备注","type"=>0,"trueIndex"=>7),
		array("id"=>9,"name"=>"清单编制","description"=>"描述","prev"=>8,"next"=>10,"prevParallel"=>0,"nextParallel"=>0,"remark"=>"备注","type"=>0,"trueIndex"=>8),
		array("id"=>10,"name"=>"财审中心确定招标最高限价","description"=>"描述","prev"=>9,"next"=>11,"prevParallel"=>0,"nextParallel"=>0,"remark"=>"备注","type"=>0,"trueIndex"=>9),
		array("id"=>11,"name"=>"办理正式立项手续","description"=>"描述","prev"=>10,"next"=>12,"prevParallel"=>0,"nextParallel"=>0,"remark"=>"备注","type"=>0,"trueIndex"=>10),
		array("id"=>12,"name"=>"施工招标","description"=>"描述","prev"=>11,"next"=>13,"prevParallel"=>0,"nextParallel"=>21,"remark"=>"备注","type"=>0,"trueIndex"=>11),
		//\**/
		array("id"=>21,"name"=>"监理招标","description"=>"描述","prev"=>8,"next"=>0,"prevParallel"=>12,"nextParallel"=>0,"remark"=>"备注","type"=>0,"trueIndex"=>11),
		///**\
		array("id"=>13,"name"=>"办理安监、质检及施工手续","description"=>"描述","prev"=>12,"next"=>14,"prevParallel"=>0,"nextParallel"=>22,"remark"=>"备注","type"=>0,"trueIndex"=>12),
		array("id"=>14,"name"=>"施工单位、监理人员进场施工","description"=>"描述","prev"=>13,"next"=>15,"prevParallel"=>0,"nextParallel"=>0,"remark"=>"备注","type"=>0,"trueIndex"=>13),
		array("id"=>15,"name"=>"总体竣工验收","description"=>"描述","prev"=>14,"next"=>16,"prevParallel"=>0,"nextParallel"=>0,"remark"=>"备注","type"=>0,"trueIndex"=>14),
		array("id"=>16,"name"=>"移交","description"=>"描述","prev"=>15,"next"=>17,"prevParallel"=>0,"nextParallel"=>0,"remark"=>"备注","type"=>0,"trueIndex"=>15),
		array("id"=>17,"name"=>"审计","description"=>"描述","prev"=>16,"next"=>0,"prevParallel"=>0,"nextParallel"=>0,"remark"=>"备注","type"=>0,"trueIndex"=>16),
	);//固定阶段

	public function getFixedProcess($projid)
	{
		foreach ($this->fixedProcess as $key => $value) {
			$this->fixedProcess[$key]["pid"] = $projid;
		}
		return $this->fixedProcess;
	}

	public function setFixedProcess($newFixedProcess)
	{
		$this->fixedProcess = $newFixedProcess;
	}

	public function getCurrentPhase()
	{
		return $_SESSION["curphase"];
	}

	public function createProjPhase($projId,$phaseArr)
	{
		$this->startTrans();

		foreach($phaseArr as $value)
		{
			if(!$this->add($value))
			{
				$this->rollback();
				return false;
			}
		}

		$this->commit();
		return true;
	}

	public function updateProjPhase($projId,$phaseArr)
	{
		$this->startTrans();
		
		if(!$this->where(array("pid"=>$projId))->delete())
		{
			$this->rollback();
			return false;
		}

		foreach($phaseArr as $value)
		{
			if(!$this->add($value))
			{
				$this->rollback();
				return false;
			}
		}
		$this->commit();
		return true;
	}

	/*//初始化固定流程和预期流程
	public function initProjPhase($projId)
	{
		if(empty($projId))
		{
			$this->error = "项目ID不合法";
			return false;
		}
		foreach ($this->fixedProcess as $key=>$value) {
			$this->fixedProcess[$key]["pid"] = $projId;
		}
		$this->startTrans();
		//初始化固定流程
		foreach ($this->fixedProcess as $key=>$value) {
			if(!$this->add($value))
			{
				$this->rollback();
				return false;
			}
		}
		//初始化预期流程
		foreach ($this->fixedProcess as $key=>$value) {
			$this->fixedProcess[$key]["type"] = 1;
		}
		foreach ($this->fixedProcess as $key=>$value) {
			if(!$this->add($value))
			{
				$this->rollback();
				return false;
			}
		}
		$this->commit();
		return true;
	}

	public function createProjPhase($phaseArr)
	{
		$prevId = $phaseArr["prev"];
		$nextId = $phaseArr["next"];
		$prevParallel = $phaseArr["prevParallel"];
		$nextParallel = $phaseArr["nextParallel"];

		if($prevId!=0)
		{
			if(!$this->where(array("id"=>$prevId))->save(array("next"=>$phaseArr["id"])))
				return false;
		}	

		if($nextId!=0)
		{
			if(!$this->where(array("id"=>$nextId))->save(array("prev"=>$phaseArr["id"])))
				return false;
		}

		if($prevParallel!=0)
		{
			if(!$this->where(array("id"=>$prevParallel))->save(array("nextParallel"=>$phaseArr["id"])))
				return false;
		}	

		if($nextParallel!=0)
		{
			if(!$this->where(array("id"=>$nextParallel))->save(array("prevParallel"=>$phaseArr["id"])))
				return false;
		}

		if(!$this->add($phaseArr))
			return false;
		return true;
	}

	public function deleteProjPhase($phaseArr)
	{

		$prevId = $phaseArr["prev"];
		$nextId = $phaseArr["next"];
		$prevParallel = $phaseArr["prevParallel"];
		$nextParallel = $phaseArr["nextParallel"];

		if($prevId!=0)
		{
			if(!$this->where(array("id"=>$prevId))->save(array("next"=>$nextId)))
				return false;
		}	

		if($nextId!=0)
		{
			if(!$this->where(array("id"=>$nextId))->save(array("prev"=>$prevId)))
				return false;
		}

		if($prevParallel!=0)
		{
			if(!$this->where(array("id"=>$prevParallel))->save(array("nextParallel"=>$nextParallel)))
				return false;
		}	

		if($nextParallel!=0)
		{
			if(!$this->where(array("id"=>$nextParallel))->save(array("prevParallel"=>$prevParallel)))
				return false;
			if($this->isMain($phaseArr))
			{
				if(!$this->where(array("id"=>$nextParallel))->save(array("prev"=>$prevId)))
					return false;
				if(!$this->where(array("id"=>$nextParallel))->save(array("next"=>$nextId)))
					return false;	
				if(!$this->where(array("id"=>$prevId))->save(array("next"=>$nextParallel)))
					return false;
				if(!$this->where(array("id"=>$nextId))->save(array("prev"=>$nextParallel)))
					return false;	
			}
		}

		if(!$this->where(array("id"=>$phaseArr["id"]))->delete())
			return false;
		return true;
	}

	private function isMain($phase)
	{
		if($phase['prev']!=0||$phase['next']!=0)
		{
			return true;
		}
		else
			return false;
	}*/
} 