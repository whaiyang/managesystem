<?php
namespace Home\Controller;
use Think\Controller;

class SearchController extends BaseController
{
	public function index()
	{
		$this->display();
	}


	public function result()
	{
		if(IS_POST)
		{
			$project_name = $_POST['project_name'];
			$project_address = $_POST['project_address'];
			$project_fund = $_POST['project_fund'];
			$project_start_date = strtotime($_POST['project_start_date']);
			$project_end_date = strtotime($_POST['project_end_date']);
			$project_accordance = $_POST['project_accordance'];
			$project_source_of_funds = $_POST['project_source_of_funds'];
			$project_progress = $_POST['project_progress'];
			$project = D('Search');
			$return = $project->searchProject(	$project_name,
												$project_address,
												$project_fund,
												$project_start_date,
												$project_end_date, 
												$project_accordance,
												$project_source_of_funds, 
												$project_progress);

			$progress = D('Project');
			foreach ($return as  $key => $value) {
				$return[$key]['progress'] = $progress->getProjProgress($value['id']);
				$return[$key]['phase_name'] = $project->phaseIdToName($value['phase_id']);
			}
			$this->assign("searchResult", $return);
			
		}
		$this->display();
	}
}