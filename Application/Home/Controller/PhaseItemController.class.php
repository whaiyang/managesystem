<?php
namespace Home\Controller;
use Think\Controller;

class PhaseItemController extends BaseController
{
	private $key = array();
	private $value = array();
	private $curIndex = 0;

	public function index()
	{
		$Phase_item = D("PhaseItem");
		if(IS_POST)
		{
			$User = D("User");
			if(!$User->hasPrivilege(array("projects"=>array($_GET['project_id']=>array("normal")))))
			{
				$this->error("无权限");
				exit;
			}
			
			$this->initPost();
			$Phase_item->where(array("project_id"=>$_GET['project_id'],"phase_id"=>$_GET['phase_id']))->delete();
			while($this->getNext())
			{
				if(!$Phase_item->create())
					$this->error("编辑出错了");
				else
				{	
					if(!$Phase_item->add())
						$this->error("编辑出错了");	
				}
			}
			//用户历史记录
			D("User")->addHistory("编辑了流程基本信息，流程id：".$_GET['phase_id']);

			$this->success("编辑成功");
		}
		else
		{
			$phase_item_list = $Phase_item->where(array("project_id"=>$_GET['project_id'],"phase_id"=>$_GET['phase_id']))->order("id asc")->select();
			$this->assign("plist",$phase_item_list);
			$this->assign("projid",$_GET['project_id']);
			$this->assign("phaseid",$_GET['phase_id']);
			$this->display();
		}
	}

	private function initPost()
	{
		$this->key = $_POST["name"];
		$this->value = $_POST["value"];

		$_POST["name"] = null;
		$_POST["value"] = null;
	}

	private function getNext()
	{
		if($this->curIndex<count($this->key))
		{
			$_POST["name"] = $this->key[$this->curIndex];
			$_POST["value"] = $this->value[$this->curIndex];

			$this->curIndex++;
			return true;
		}
		else
			return false;
	}
}