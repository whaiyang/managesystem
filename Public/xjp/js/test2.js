$(document).ready(function() {
        
        /**************add a new line******************/
        $('.row-add').click(function () {
            var rownum= $('#EditTable')[0].rows.length-1;
            var newtr='<tr><td><a>'+rownum+'</a></td>';
            var newtd='<td><a href="#" class="table-data"></a></td>';
            for (var i = 0; i < colnum; i++) {
              newtr+=newtd;
            }
            $('#EditTable>tbody').append(newtr+'</tr>');
            $('.table-data').editable({ 
                            type: 'text',
                            title: 'Enter Value'
                          }); 

        });

  
        /*************creat the table head with array*******************/
        function isEmptyObject(obj){
            for(var n in obj){return false} 
            return true; 
        } 
 
        function creattablehead (menuarray) {
            var tempmenu1_r='<th rowspan="2">';
            var tempmenu1_c='<th colspan="2">';
            var tempmenu2='<th>';
            $('#EditTable>thead>tr:eq(0)').append('<th rowspan="2">#</tr>');
            $.each(menuarray,function (key,value) {
                if( isEmptyObject(value) ) {
                    $('#EditTable>thead>tr:eq(0)').append(tempmenu1_r+key+'</tr>');
                    colnum++;
                }else{
                    $('#EditTable>thead>tr:eq(0)').append(tempmenu1_c+key+'</tr>');
                    $.each(value,function  (key2) {
                        $('#EditTable>thead>tr:eq(1)').append(tempmenu2+key2+'</tr>');
                        colnum++;
                    });
                }
            });
        }

        var testmenu=$('#EditTable>thead').attr('xjp-menu');
       // var testmenu="{'aaa':{},'bbb':{'ccc':{},'ddddd':{}},'eeeee':{},'ffff':{}}";
        var jsonmenu=eval('(' + testmenu + ')');
        var colnum=0;     
        creattablehead(jsonmenu);


        /*************fill in the table with array*******************/

        function fillintable (valuearray) {
            var rownum=valuearray[0].length;
            var newtd='<td><a href="#" class="table-data">';
            for (var i = 0; i < rownum; i++) {
                var newtr='<tr><td><a>'+(i+1)+'</a></td>';
                for (var j = 0; j < colnum; j++) {
                    if (!valuearray[j]) {valuearray[j]=[];};
                    console.log(valuearray[j][i])
                    var valtemp=valuearray[j][i] || '';

                    newtr+=(newtd+valtemp+'</a></td>');
                }
                $('#EditTable>tbody').append(newtr+'</tr>');
            };
        }

        var testvalue=$('#EditTable').attr('xjp-value');
        //var testvalue="[['bbb','ccc'],['bbb','ccc'],['bbb','ccc'],['bbb','ccc']]";
        if (testvalue=="") {testvalue='[[]]';}; 
        var jsonvalue=eval('(' + testvalue + ')');
        fillintable(jsonvalue);
      

        /***************editable***********************/

        $('.table-data').editable({ 
                        type: 'text',
                        title: 'Enter Value'
                      }); 


        /*************get the value from the table***********/
        function gettablevalue () {
            var arrayvalue=[];
            var alltr=$('#EditTable>tbody>tr');
            for (var i = 0; i < alltr.length; i++) {
                var oneoftr=$(alltr[i])[0];
                var aforow=$(oneoftr).find('td>a');
                 for (var j = 0; j < $(aforow).length-1; j++) {
                    var oneofa=$(aforow)[j+1];
                    if (typeof(arrayvalue[j])==='undefined') {
                        arrayvalue[j]=[];
                    };
                    if ($(oneofa).hasClass('editable-empty')) {
                        arrayvalue[j][i]='';
                    }else{
                        arrayvalue[j][i]=$(oneofa).html();
                    }
                }
            };
            return arrayvalue;
        }


        /*********submit the table value *****************/
         $('#submitform>.btn-submit').click(function(){
            var tablevalue=gettablevalue();

            /************ecode from tablevalue *********/
            var valuejson=JSON.stringify(tablevalue);

            $('#submitform>#newvalue').val(valuejson);
            $('#submitform').submit(); 
            // $('#submitform').submit(function () {
            //     alert('\u8868\u683C\u63D0\u4EA4\u6210\u529F\uFF01');
            //  });
         });
  });