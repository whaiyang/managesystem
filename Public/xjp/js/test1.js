 $(document).ready(function  () {

        /*****rm a menu ******/
        $('body').on('click','.rmmenu-btn',function(){
            $(this).parents('.menu').next('.menu-2-box').remove();
            $(this).parents('.menu').remove();
         })


        /**********add a main menu *************/
         var menu1temp='<div class="input-group menu menu-1"><input type="text" class="form-control"><span class="input-group-btn"><button class="btn btn-danger rmmenu-btn" type="button"><span class="glyphicon glyphicon-remove"></span></button><button class="btn btn-default  addmenu-btn" type="button"><span class="glyphicon glyphicon-plus"></span></button></span></div>';
         $('body').on('click','.addmainmenu',function () {
            $(this).before(menu1temp);
            $(this).prev('.menu').find('input').focus();
          });


        /**********add a sub menu *************/
        var menu2temp='<div class="input-group menu menu-2"><input type="text" class="form-control"><span class="input-group-btn"><button class="btn btn-danger rmmenu-btn" type="button"><span class="glyphicon glyphicon-remove"></span></button></span></div>';
        
         $('body').on('click','.addmenu-btn',function () {
              var menuboxtemp='<div class="menu-2-box"></div>';
              var menubox= $(this).parents('.menu').next('.menu-2-box');
              if (menubox.length==0) {
                 $(this).parents('.menu').after(menuboxtemp);
              };
              menubox= $(this).parents('.menu').next('.menu-2-box');
              menubox.append(menu2temp);
              menubox.find('.menu:last-child>input').focus();
          });



         /********get the values from the inputs into a array ********************/
         function menutoarray () {
           var ArrayMenu={};
           var menu1=$('.menu-1');
           for (var i = 0; i < menu1.length; i++) {
              var temp=$(menu1[i]).find('input').val();
              var menubox= $(menu1[i]).next('.menu-2-box');
                ArrayMenu[temp]={};
              if (menubox.length!=0) {
                var menu2=menubox.find('.menu-2');
                for (var j = 0; j < menu2.length; j++) {
                  var temp2=$(menu2[j]).find('input').val();
                  ArrayMenu[temp][temp2]={};
                }
              }
           }
           return ArrayMenu;
         }

       
         /*********submit the form *****************/
         $('#submitform>.btn-submit').click(function(){
          
            var menu=menutoarray();
            var menujson=JSON.stringify(menu);

            $('#submitform>#newmenu').val(menujson);
           
            // $('#submitform').submit(function () {
            //      alert('\u8868\u5934\u63D0\u4EA4\u6210\u529F\uFF01');
            //   });
         });
      });