<?php
namespace Home\Model;
use Think\Model;

//数据库中以链表方式存储
class FileManageModel extends Model
{
	//文件下载
	public function downloadFile($savename)
	{
		
		if(empty($savename)){
			return false;
		}
		$model = M('file');
		$info = $model->where("path=%d",$savename)->find();
		$uploadpath='./Public/uploads';
		return array("path"=>$uploadpath.'/'.$savename.'.'.$info['ext'],"name"=>$info["name"].".".$info['ext']);
	}

	//文件上传
	public function uploadFile($phase_id, $project_id, $category_id,$name, $type){
	    
	    $upload = new \Think\Upload();// 实例化上传类
	    $upload->maxSize   	=     0 ;// 设置附件上传大小
	    $upload->exts      	=     array('jpg', 'gif', 'png', 'jpeg', 'pdf', 'psd', 'ico', 'rar', 'zip', '7z', 'ppt', 'pptx', 'doc', 'docx', 'xls', 'xlsx', 'pub', 'vsd', 'txt');// 设置附件上传类型
	    $upload->rootPath  	=      './Public/uploads/'; // 设置附件上传根目录
	    $upload->autoSub  	=		false;
	    $upload->saveName 	= 	   time().'_'.mt_rand();
	    $savename 	=	$upload->saveName;
	    // 上传单个文件 
	    $info   =   $upload->upload();

	    if(!$info) {// 上传错误提示错误信息
	        $this->error = $upload->getError();
	        return false;
	    }else{// 上传成功 获取上传文件信息
	    	$model = M('file');
	    	$data['phase_id']		=	$phase_id;
	    	$data['project_id']		=	$project_id;
	    	$data['category_id']	=	$category_id;
	    	$data['name']			=	$name;
	        $data['path']			=	$savename;
	        $data['uploadtime']		= 	NOW_TIME;
	        $data['ext']			= 	$info['inputFile']['ext'];
	        $data['type']			=	$type;
	        $model->add($data);
	        return true;
	    }
	}

	public function deleteFile($savename)
	{
		if(empty($savename)){
			return false;
		}
		$path = $_SERVER['DOCUMENT_ROOT'].'managesystem/Public/uploads'.$savename;
		$model = M('file');

		$info = $model->where("path=%d",$savename)->find();
		$uploadpath=$_SERVER['DOCUMENT_ROOT'].'managesystem/Public/uploads'.'/'.$savename.'.'.$info['ext'];
		$model->where(array("path"=>$savename))->delete();
		unlink('Public/uploads/'.$savename.'.'.$info['ext']);
		
		return true;
	}

	//按项目查找
	public function findFileWithProject($project_id,$category_id){
		if(empty($project_id)){
			return false;
		}
		$model = M('file');
		return  $model->where("project_id=%d and category_id=%d",$project_id,$category_id)->select();
	}

	//按流程查找
	public function findFileWithPhase($phase_id){
		if(empty($phase_id)){
			return false;
		}
		$model = M('file');
		return $model->where("phase_id=%d",$phase_id)->select();
	}


} 