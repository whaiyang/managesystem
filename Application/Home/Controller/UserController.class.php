<?php
namespace Home\Controller;
use Think\Controller;

class UserController extends BaseController
{

	public function index()
	{
		$User = D("User");
		if(!$User->isAdmin())
		{
			echo "无权限";
			exit;
		}

		$user = D("User");
		$userList = $user->table("ms_user u,ms_user_group g")->field("u.*,g.name")->where("u.group_id=g.group_id")->select();
		$this->assign("ulist",$userList);
		$this->display();
	}

	public function register()
	{
		$user = D("User");

		if(!$user->isAdmin())
		{
			echo "无权限";
			exit;
		}

		if(IS_POST)
		{
			if(!$user->create())
				$this->error($user->getError());
			else
			{
				if(!$user->add())
					$this->error($user->getError());	
				else
				{
					//用户历史记录
					D("User")->addHistory("添加了用户".$_POST['username']);

					$this->success("添加成功",U("User/index"));
				}
			}
		}
		else
		{
			$gdata = D("UserGroup")->where("")->select();
			$this->assign("gdata",$gdata);
			$this->display();
		}
	}

	public function edit()
	{
		$user = D("User");

		if(!$user->isAdmin())
		{
			echo "无权限";
			exit;
		}

		if(!$this->check_userid())
			return;
		if(IS_POST)
		{ 
			if(!empty($_POST["password"]))
				$_POST["password"] = md5($_POST["password"]);
			if(!$user->create($_POST,2))
				$this->error($user->getError());
			else
			{
				if(!$user->save())
					$this->error($user->getError(),U("User/index"));	
				else
				{
					//用户历史记录
					D("User")->addHistory("编辑了用户:".$_POST['username']);

					$this->success("编辑成功",U("User/index"));
				}
			}
		}
		else
		{
			$userdata = $user->where(array("user_id"=>$_GET["user_id"]))->find();
			$this->assign("udata",$userdata);
			$this->display();
		}
	}

	public function advance_edit()
	{
		$user = D("User");

		if(!$user->isAdmin())
		{
			echo "无权限";
			exit;
		}

		if(!$this->check_userid())
			return;

		if(IS_POST)
		{
			if(!empty($_POST["password"]))
				$_POST["password"] = md5($_POST["password"]);
			if(!$user->create($_POST,2))
				$this->error($user->getError(),U("User/index"));
			else
			{
				if(!$user->save())
					$this->error($user->getError(),U("User/index"));	
				else
				{
					//用户历史记录
					D("User")->addHistory("在高级模式下编辑了用户:".$_POST['username']);

					$this->success("编辑成功",U("User/index"));
				}
			}
		}
		else
		{
			$userdata = $user->where(array("user_id"=>$_GET["user_id"]))->find();
			$gdata = D("UserGroup")->where("")->select();
			$this->assign("udata",$userdata);
			$this->assign("gdata",$gdata);
			$this->display();
		}
	}

	public function history()
	{
		$User = D("User");
		if(!$User->isAdmin())
		{
			echo "无权限";
			exit;
		}

		$Page  = new \Think\Page(M("history")->count(),100);
		$show  = $Page->show();
		$history = M("history")->limit($Page->firstRow.','.$Page->listRows)->order("id desc")->select();
		$this->assign("page",$show);
		$this->assign("history",$history);
		$this->display();
	}

	public function delete()
	{
		$user = D("User");
		if(!$user->isAdmin())
		{
			echo "无权限";
			exit;
		}

		if(!$this->check_userid())
			return;
		
		$username = $user->where(array("user_id"=>$_GET['user_id']))->find();
		$username = $username["username"];
		if($user->deleteUser($_GET['user_id']))
		{
			//用户历史记录
			D("User")->addHistory("删除了用户，名字：".$username);

			$this->success("删除成功",U("User/index"),U("User/index"));
		}
		else
			$this->error("删除失败",U("User/index"),U("User/index"));
	}

	/*public function addHistory()
	{
		$h = D("User");
		$h->addHistory($_GET["h"]);
	}*/	

	private function check_userid()
	{
		$user_id = $_GET["user_id"];
		if(empty($user_id))
		{
			$this->error("数据错误,user_id未指定");
			return false;
		}
		else
		{
			$_POST["user_id"] = $user_id;
			return true;
		}
	}
}