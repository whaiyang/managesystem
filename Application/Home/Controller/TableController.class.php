<?php
namespace Home\Controller;
use Think\Controller;

/*
	tableList ----createTemplate *								创建模板
			  ----templateList-------display_edit-----edit *	从模板建表
			  ----listSelect------createExcel *					生成excel
	    ----display_edit										编辑表格
*/
class TableController extends BaseController
{
	public function tableList()
	{
		$Table = D("Table");
		$Tablelist = null;
		if(empty($_GET["project_id"]))
		{
			$this->error("请先选择项目");
		}
		if(!empty($_GET['category_id']))
		{
			$Tablelist = $Table->where(array("category_id"=>$_GET['category_id'],"project_id"=>$_GET["project_id"],"type"=>0))->order("id desc")->select();
		}
		else if(!empty($_GET['phase_id']))
		{
			$Tablelist = $Table->where(array('phase_id'=>$_GET['phase_id'],"type"=>0))->order("id desc")->select();
		}
		else
		{
			$this->error("Unkown type");
		}
		
		$this->assignEssentialData();
		$this->assign("tablelist",$Tablelist);
		$this->display();
	}

	public function listSelect()
	{
		$Table = D("Table");
		$Tablelist = null;
		if(empty($_GET["project_id"]))
		{
			$this->error("请先选择项目");
		}
		if(!empty($_GET['category_id']))
		{
			$Tablelist = $Table->where(array("category_id"=>$_GET['category_id'],"project_id"=>$_GET["project_id"],"type"=>0))->order("id desc")->select();
		}
		else if(!empty($_GET['phase_id']))
		{
			$Tablelist = $Table->where(array('phase_id'=>$_GET['phase_id'],"type"=>0))->order("id desc")->select();
		}
		else
		{
			$this->error("Unkown type");
		}
		
		$this->assignEssentialData();
		$this->assign("tablelist",$Tablelist);
		$this->display();
	}

	public function edit()
	{
		if(IS_POST)
		{
			$project_id = $_GET['project_id'];
			$phase_id = $_GET['phase_id'];
			$category_id = $_GET['category_id'];
			$this->essentialGetToPost();
			$Table = D("Table");
			if($_GET['type']=="create")
			{	
				if(!$Table->create())
				{
					$this->error("创建表格数据失败</br>".$Table->getError());
				}
				else
				{
					if(!$Table->add())
					{
						$this->error("编辑失败</br>".$Table->getError());
					}
					else
					{
						//用户历史记录
						D("User")->addHistory("创建了表格，名字：".$_POST['name']);
						$this->assign("jumpUrl",U("Table/tableList?project_id=$project_id&phase_id=$phase_id&category_id=$category_id"));
						$this->success("创建成功");
					}
				}
			}
			else if($_GET['type']=="edit")
			{	
				if(!$Table->create())
				{
					$this->error("创建表格数据失败</br>".$Table->getError());
				}
				else
				{
					if(!$Table->save())
					{
						$this->error("编辑失败</br>".$Table->getError());
					}
					else
					{
						//用户历史记录
						D("User")->addHistory("编辑了表格，名字：".$_POST['name']);
						$this->assign("jumpUrl",U("Table/tableList?project_id=$project_id&phase_id=$phase_id&category_id=$category_id"));
						$this->success("编辑成功");
					}
				}
			}
		}
	}

	public function display_edit()
	{
		if($_GET['type']=="create")
		{
			if(empty($_POST["id"]))
			{
				$this->error("请选择模板");
				exit;
			}

			$Table = D("Table");
			$tableData = $Table->where(array("id"=>$_POST['id']))->find();
			$this->assign("tpldata",$tableData);
		}
		else if($_GET['type']=="edit")
		{
			$Table = D("Table");
			$tableData = $Table->where(array("id"=>$_GET['id']))->find();
			$tplData = $Table->where(array("id"=>$tableData['tpl_id']))->find();
			$this->assign("tabledata",$tableData);
			$this->assign("tpldata",$tplData);
		}

		$this->assignEssentialData();
		$this->display();
	}

	public function templateList()
	{
		$templatelist = D("Table")->where(array("type"=>1))->order("id desc")->select();

		$this->assignEssentialData();
		$this->assign("templateList",$templatelist); 
		$this->display();
	}

	public function createTemplate()
	{
		if(IS_POST)
		{
			$_POST['type'] = 1;
			if(empty($_GET["project_id"]))
			{	
				$this->error("请先选择项目");
			}
			if(!empty($_GET['category_id']))
			{
				unset($_POST['phase_id']);
			}
			else if(!empty($_GET['phase_id']))
			{
				unset($_POST['category_id']);
			}
			else
			{
				$this->error("Unkown type");
			}

			if(empty($_POST['table_head']))
			{
				$this->error("请先编辑模板，不能提交空模板");
			}

			$this->essentialGetToPost();
			$_POST['type'] = 1;//模板

			$Table = D("Table");
			//var_dump($_POST);exit;
			if(!$Table->create())
				$this->error("创建模板数据失败</br>".$Table->getError());
			else
			{
				if(!$Table->add())
					$this->error("添加失败</br>".$Table->getError());
				else
				{
					//用户历史记录
					D("User")->addHistory("创建了表格模板，名字：".$_POST['name']);
					$project_id = $_GET['project_id'];
					$phase_id = $_GET['phase_id'];
					$category_id = $_GET['category_id'];
					$this->assign("jumpUrl",U("Table/tableList?project_id=$project_id&phase_id=$phase_id&category_id=$category_id"));
					$this->success("创建成功");
				}
			}
		}
		else
		{
			$this->assignEssentialData();
			$this->display();
		}
	}

	public function deleteTable()
	{
		if(empty($_GET["id"]))
		{
			$this->error("请选择报表");
			exit;
		}
		$Table = D("Table");
		$name = $Table->where(array('id'=>$_GET["id"]))->find();
		$name = $name['name'];
		if(!$Table->where(array('id'=>$_GET["id"]))->delete())
		{
			$this->error("删除失败</br>".$Table->getError());
			exit;
		}
		else
		{
			//用户历史记录
			D("User")->addHistory("删除了表格，名字：".$name);

			$project_id = $_GET['project_id'];
			$phase_id = $_GET['phase_id'];
			$category_id = $_GET['category_id'];
			$this->assign("jumpUrl",U("Table/tableList?project_id=$project_id&phase_id=$phase_id&category_id=$category_id"));
			$this->success("删除成功");	
		}
	}

	public function createExcel()
	{
		if(empty($_POST["id"]))
		{
			$this->error("请选择报表");
			exit;
		}

		$Table = D("Table");

		$TablesData = $Table->where(array("id"=>array("in",$_POST["id"])))->select();

		$tables = array();
		foreach($TablesData as $key=>$value)
		{
			$tpl = $Table->where(array("id"=>$value['tpl_id']))->find();
			$tpl['table_head'] = str_replace("&quot;", '"', $tpl['table_head']);
			$value['table_content'] = str_replace("&quot;", '"', $value['table_content']);
			$tables[$value['name']] = array(json_decode($tpl['table_head']),json_decode($value['table_content']));
		}
		$tables = json_encode($tables);
		$this->assignEssentialData();
		$this->assign("tables",$tables);
		$this->display();
	}

	public function downloadExcel()
	{
		$head = "";
		$content = "";
		$filename = "";
		if($_GET['type']=="create")
		{
			$head = json_decode($_POST['head']);
			$content = json_decode($_POST['content']);
			$filename = $_POST["name"];
		}
		else if($_GET['type']=="download")
		{
			$Table = D("Table");
			$TableData = $Table->where(array("id"=>$_GET['id']))->find();
			$HeadData = $Table->where(array("id"=>$TableData['tpl_id']))->find();
			$head = json_decode(str_replace("&quot;", '"',$HeadData['table_head']));
			$content = json_decode(str_replace("&quot;", '"',$TableData['table_content']));
			$filename = $TableData['name'];
		}

		//var_dump($head);echo "</br>";
		//var_dump($content);echo "</br>";
		//echo $name;

		$this->exportexcel($content,$head,$filename);
	}

	private function assignEssentialData()
	{
		$this->assign("project_id",$_GET["project_id"]);
		$this->assign("phase_id",$_GET['phase_id']);
		$this->assign("category_id",$_GET['category_id']);
	}

	private function essentialGetToPost()
	{
		$_POST['project_id'] = intval($_GET['project_id']);
		$_POST['category_id'] = intval($_GET['category_id']);
		$_POST['phase_id'] = intval($_GET['phase_id']);
	}

	private function exportexcel($data=array(),$title=array(),$filename='noname')
	{
    	header("Content-type:application/octet-stream");
    	header("Accept-Ranges:bytes");
    	header("Content-type:application/vnd.ms-excel");  
    	header("Content-Disposition:attachment;filename=".$filename.".xls");
    	header("Pragma: no-cache");
    	header("Expires: 0");
    	//导出xls 开始
    	$rowspan = 0;
    	$excelStr = "<table width='100%' border='1' cellspacing='5' cellpadding='2'><thead>";
    	$secHead = array();
    	foreach($title as $value)
    	{
    		if(count($value>0))
    		{
    			$rowspan = 2;
    			break;
    		}
    	}

    	$excelStr .= "<tr>";
    	foreach($title as $key=>$value)
    	{
    		$col = 0;
    		$secValue = array();
    		foreach($value as $key2=>$value)
    		{
    			$secValue[] = $key2;
    			$col++;
    		}
    		if($col>0)
    		{
    			$secHead = array_merge($secHead,$secValue);
    			$excelStr .= "<th colspan='".$col."'>$key</th>";
    		}
    		else
    		{
    			if($rowspan==0)
    			{
    				$excelStr .= "<th>$key</th>";
    			}
    			else
    			{
    				$excelStr .= "<th rowspan='".$rowspan."'>$key</th>";
    			}
    		}
    	}
    	$excelStr .= "</tr>";
    	//var_dump($secHead);
    	if(!empty($secHead))
    	{
    		$excelStr .= "<tr>";
    		foreach($secHead as $key=>$value)
    		{
    			$excelStr .= "<th>$value</th>";
    		}
    		$excelStr .= "</tr>";
    	}
    	$excelStr .= "</thead>";

    	$rowNum = count($data);
    	$colIndex = 0;
    	$maxCol = count($data[0]);

    	while($colIndex<$maxCol)
    	{
    		$excelStr .= "<tr>";
    		for($i=0;$i<$rowNum;$i++)
    		{
    			$excelStr .= "<td>".$data[$i][$colIndex]."</td>";
    		}
    		$colIndex++;
    		$excelStr .= "</tr>";
    	}

    	$excelStr .= "</table>";

    	echo $excelStr;
 	}

 	private function exportexcel2($data=array(),$title=array(),$filename='noname')
	{
    	header("Content-type:application/octet-stream");
    	header("Accept-Ranges:bytes");
    	header("Content-type:application/vnd.ms-excel");  
    	header("Content-Disposition:attachment;filename=".$filename.".xls");
    	header("Pragma: no-cache");
    	header("Expires: 0");
    	//导出xls 开始
    	$rowspan = 0;
    	$secHead = array();
    	foreach($title as $value)
    	{
    		if(count($value>0))
    		{
    			$rowspan = 2;
    			break;
    		}
    	}

    	foreach($title as $key=>$value)
    	{
    		$col = 0;
    		$secValue = array();
    		foreach($value as $key2=>$value)
    		{
    			$secValue[] = $key2;
    			$col++;
    		}
    		if($col>0)
    		{
    			$secHead = array_merge($secHead,$secValue);
    			$excelStr .= $key.$this->getT($col);
    		}
    		else
    		{
    				$excelStr .= $key."\t";
    		}
    	}
    	$excelStr .= "\n";
    	//var_dump($secHead);
    	if(!empty($secHead))
    	{
    		foreach($secHead as $key=>$value)
    		{
    			$excelStr .= $value."\t";
    		}
    		$excelStr .= "\n";
    	}

    	$rowNum = count($data);
    	$colIndex = 0;
    	$maxCol = count($data[0]);

    	while($colIndex<$maxCol)
    	{
    		for($i=0;$i<$rowNum;$i++)
    		{
    			$excelStr .= $data[$i][$colIndex]."\t";
    		}
    		$colIndex++;
    		$excelStr .= "\n";
    	}

    	echo $excelStr;
 	}

 	private function getT($num)
 	{
 		$tmp = "";
 		while($num-->0)
 			$tmp .= "\t";
 		return $tmp;
 	}
}